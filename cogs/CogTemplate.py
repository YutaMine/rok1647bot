import discord
from discord.ext import commands
from discord_slash import cog_ext, SlashContext


class CogTemplate(commands.Cog):

    # コンストラクタ
    def __init__(self, bot):
        self.bot = bot

        @bot.event
        async def on_message(message):
            # ボットのメッセージは無視する。
            if message.author == self.bot.user:
                return

            # コマンドprefixがある場合、コマンドとして処理する。
            if message.content.startswith(self.bot.command_prefix):
                await self.bot.process_commands(message)
                return

            # コマンドprefixがない場合、on_messageイベントとして処理する。
            return

    @cog_ext.cog_slash(name="test", description="this is test command")
    async def _test(self, ctx: SlashContext):
        embed = discord.Embed(title="embed test")
        await ctx.send(content="test", embeds=[embed])


def setup(bot):
    bot.add_cog(CogTemplate(bot))
