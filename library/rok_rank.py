from enum import Enum, IntEnum


class ColIndex(IntEnum):
    POWER_BOTTOM = 0
    POWER_TOP = 1
    DEATH_QUOTA = 2


class RankIndex(IntEnum):
    RANK_1 = 1
    RANK_2 = 2
    RANK_3 = 3
    RANK_4 = 4
    RANK_5 = 5
    RANK_6 = 6
    RANK_7 = 7
    RANK_8 = 8


# ノルマ表[戦力下限、戦力上限、戦死ノルマ]
RANK_INFO = {
    RankIndex.RANK_1: [140000000, 999999999, 8810000],
    RankIndex.RANK_2: [120000000, 139999999, 6560000],
    RankIndex.RANK_3: [100000000, 119999999, 4310000],
    RankIndex.RANK_4: [90000000, 99999999, 3410000],
    RankIndex.RANK_5: [80000000, 89999999, 2510000],
    RankIndex.RANK_6: [70000000, 79999999, 1160000],
    RankIndex.RANK_7: [60000000, 69999999, 400000],
    RankIndex.RANK_8: [0, 59999999, 100000]
}


class RankInfo(object):
    def __init__(self, rank: RankIndex):
        if rank not in RankIndex:
            raise Exception("Invalid RankIndex")
        rank_info = RANK_INFO[rank]
        self.power_top = rank_info[ColIndex.POWER_TOP]
        self.power_bottom = rank_info[ColIndex.POWER_BOTTOM]
        self.death_quota = rank_info[ColIndex.DEATH_QUOTA]


def get_rank_info(rank: RankIndex, col_index: ColIndex) -> float:
    if rank not in RANK_INFO:
        raise Exception("Invalid rank")
    if col_index not in ColIndex:
        raise Exception("Invalid col_index")
    return RANK_INFO[rank][col_index]


def judge_rank(death: float):
    for rank in RankIndex:
        rank_info = RankInfo(rank)
        if death >= rank_info.death_quota:
            return rank.name
    return None
