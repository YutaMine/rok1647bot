from enum import Enum, IntEnum


class ColIndex(IntEnum):
    POWER_BOTTOM = 0
    POWER_TOP = 1
    KILL_QUOTA_POINT1 = 2
    DEATH_QUOTA_POINT1 = 3
    KILL_QUOTA_POINT2 = 4
    DEATH_QUOTA_POINT2 = 5


class RankIndex(IntEnum):
    RANK_1 = 1
    RANK_2 = 2
    RANK_3 = 3
    RANK_4 = 4
    RANK_5 = 5
    RANK_6 = 6
    RANK_7 = 7
    RANK_8 = 8


RANK_INFO = {
    RankIndex.RANK_1: [150.000000, 999.999999, 52.500000, 3.110000, 17.500000, 8.810000],
    RankIndex.RANK_2: [125.000000, 149.999999, 52.500000, 2.360000, 17.500000, 6.560000],
    RankIndex.RANK_3: [100.000000, 124.999999, 40.500000, 1.610000, 13.500000, 4.310000],
    RankIndex.RANK_4: [90.000000, 99.999999, 31.500000, 1.310000, 10.500000, 3.410000],
    RankIndex.RANK_5: [80.000000, 89.999999, 22.500000, 1.010000, 7.250000, 2.510000],
    RankIndex.RANK_6: [70.000000, 79.999999, 13.500000, 0.710000, 4.500000, 1.610000],
    RankIndex.RANK_7: [60.000000, 69.999999, 10.800000, 0.620000, 3.600000, 1.340000],
    RankIndex.RANK_8: [0, 59.999999, 8.100000, 0.530000, 2.700000, 1.070000]
}


class RankInfo(object):
    def __init__(self, rank: RankIndex):
        if rank not in RankIndex:
            raise Exception("Invalid RankIndex")
        rank_info = RANK_INFO[rank]
        self.power_top = rank_info[ColIndex.POWER_TOP]
        self.power_bottom = rank_info[ColIndex.POWER_BOTTOM]
        self.kill_quota_point1 = rank_info[ColIndex.KILL_QUOTA_POINT1] * 15
        self.death_quota_point1 = rank_info[ColIndex.DEATH_QUOTA_POINT1]
        self.kill_quota_point2 = rank_info[ColIndex.KILL_QUOTA_POINT2] * 15
        self.death_quota_point2 = rank_info[ColIndex.DEATH_QUOTA_POINT2]
        self.minimum_kill = min(self.kill_quota_point1, self.kill_quota_point2)
        self.minimum_death = min(self.death_quota_point1, self.death_quota_point2)
        self.weight = (self.kill_quota_point1 - self.kill_quota_point2) / (
                self.death_quota_point1 - self.death_quota_point2)
        self.bias = self.kill_quota_point1 - self.weight * self.death_quota_point2

    def get_min_kill(self):
        return min(self.kill_quota_point1, self.kill_quota_point2)

    def get_min_death(self):
        return min(self.death_quota_point1, self.death_quota_point2)

    def get_max_kill(self):
        return max(self.kill_quota_point1, self.kill_quota_point2)

    def get_max_death(self):
        return max(self.death_quota_point1, self.death_quota_point2)

    def get_weight(self):
        return (self.kill_quota_point1 - self.kill_quota_point2) / (self.death_quota_point1 - self.death_quota_point2)

    def get_bias(self):
        return self.kill_quota_point1 - self.get_weight() * self.death_quota_point1


def get_rank_info(rank: RankIndex, col_index: ColIndex) -> float:
    if rank not in RANK_INFO:
        raise Exception("Invalid rank")
    if col_index not in ColIndex:
        raise Exception("Invalid col_index")
    return RANK_INFO[rank][col_index]
