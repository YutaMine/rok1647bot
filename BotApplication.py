from discord.ext import commands  # Bot Commands Frameworkのインポート
import traceback  # エラー表示のためにインポート
from discord import RawReactionActionEvent
from typing import List
from discord_components import DiscordComponents

# custom module
from config import config
from library.helper import Help

# トークン取得
BOT_TOKEN: str = config.BOT_TOKEN

# 読み込むコグのリスト
INSTALL_EXTENSIONS: List[str] = [
]


# Bot本体
class KingdomBot(commands.Bot):
    """ 自作ボット """

    # Botのトークン
    TOKEN: str = None

    # Cogイベント用
    on_message_events = []
    on_raw_reaction_add_events = []
    on_raw_reaction_remove_events = []

    # コンストラクタ
    def __init__(self, command_prefix: str, token: str):
        # スーパークラスのコンストラクタンに値を渡して実行
        super().__init__(command_prefix, help_command=Help())

        self.TOKEN: str = token

        # INSTALL_COGSに格納されている名前からコグを読み込む。
        # エラーが発生した場合は、エラー内容を表示
        for cog in INSTALL_EXTENSIONS:
            try:
                self.load_extension(cog)
            except Exception:
                traceback.print_exc()

    # Cog内でon_messageを記述する際に使用するデコレータ
    def add_on_message(self, func):
        self.on_message_events.append(func)

    # Cog内でon_raw_reaction_addを記述する際に使用するデコレータ
    def add_on_raw_reaction_add(self, func):
        self.on_raw_reaction_add_events.append(func)

    # Cog内でon_raw_reaction_removeを記述する際に使用するデコレータ
    def add_on_raw_reaction_remove(self, func):
        self.on_raw_reaction_remove_events.append(func)

    # Botの起動処理
    def run(self):
        super().run(self.TOKEN)


# TranslatorBotのインスタンス化、及び起動処理
if __name__ == '__main__':
    bot = KingdomBot(command_prefix='!', token=BOT_TOKEN)


    @bot.event
    async def on_message(message):
        # ボットのメッセージは無視する。
        if message.author == bot.user:
            return

        # コマンドprefixがある場合、コマンドとして処理する。
        elif message.content.startswith(bot.command_prefix):
            await bot.process_commands(message)
            return

        # コマンドprefixがない場合、on_messageイベントとして処理する。
        else:
            # Cog内で定義したon_messageイベントを処理する。
            for func in bot.on_message_events:
                await func(message)
            return


    @bot.event
    async def on_ready():
        DiscordComponents(bot)
        print(f'We have logged in as {bot.user}')


    @bot.event
    async def on_raw_reaction_add(payload: RawReactionActionEvent):
        # リアクション付与者がBotの場合return
        if payload.member.bot:
            return

        # Cog内で定義したon_raw_reaction_addイベントを処理する。
        for func in bot.on_raw_reaction_add_events:
            await func(payload)
        return


    @bot.event
    async def on_raw_reaction_remove(payload: RawReactionActionEvent):
        # Cog内で定義したon_raw_reaction_removeイベントを処理する。
        for func in bot.on_raw_reaction_remove_events:
            await func(payload)
        return


    # bot実行
    bot.run()
