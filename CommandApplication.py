# Discord
import discord
# Data
import pandas as pd
# Web Scraping
import requests
import sympy
import traceback
from bs4 import BeautifulSoup
from datetime import datetime
from discord.ext import commands
from discord_slash import SlashCommand, SlashContext

import library.rok_rank as rok
# Setting
from config import config

bot = commands.Bot(command_prefix=config.COMMAND_PREFIX)
slash = SlashCommand(bot, sync_commands=True)

guild_ids = [
    689880757413740702,  # My Server
    795547271953252425  # 1647 All Player
]

permission_role = [
    842998172132704257,  # Officer
    843334514977800222  # 47サーバー幹部
]


@slash.slash(name="download_rok_data", guild_ids=guild_ids, description="download rok-board CSV data.")
async def _download_rok_data(ctx: SlashContext):
    # Log
    now = datetime.now()
    now = str(now)[0:19]
    print(f'command : {ctx.command}')
    print(f'time    : {now}')
    print(f'user    : {ctx.author.name}')
    print(f'process : 処理開始')

    has_permission = False
    for role in ctx.author.roles:
        if role.id in permission_role:
            has_permission = True

    if not has_permission:
        print('        : 権限がありません。')
        embed = discord.Embed(title='権限エラー', description='コマンドを実行する権限がありません。')
        await ctx.send(embeds=[embed])
        return

    try:
        # セッション開始
        session = requests.session()

        # ログイン情報
        login_info = {
            "username": config.USERNAME,
            "password": config.PASSWORD
        }

        # ログイン処理
        url = f'https://rokboard.com/j_spring_security_check'
        print('        : ROKボードにログイン中')
        res = session.post(url, data=login_info)
        res.raise_for_status()
        print(f'        : ログイン：{res.status_code}')
        await ctx.defer()

        # MyPage
        import datetime as dt
        today = dt.date.today()
        df_date: pd.DataFrame = pd.DataFrame({"upd_date": [str(today)]})
        df_date.to_json(config.UPD_DATE)

        url = f'https://rokboard.com/kvk/kingdom-details?id=ae53919f-e98c-4fc8-8532-072343a94a65&areaStartDate=2022-02-09&areaEndDate=2022-04-01&showGroups=true&showRankChanges=false&useDefaultPowerBaseline=false&showAll=true#player-rank-panel'
        print('        : リストページへ遷移中')
        res = session.get(url)
        res.raise_for_status()
        print(f'        : リストページ：{res.status_code}')

        # 解析
        print('        : htmlの解析中')
        soup = BeautifulSoup(res.text, "html.parser")
        table = soup.find_all("table", {"id": "player-rank-table"})[0]
        rows: BeautifulSoup = table.find_all("tr", {"class": "governor-row"})

        # インデックスを作成
        print("        : jsonデータへ変換中")
        index = []
        for row in rows:
            index.append(row.attrs['data-governor'])

        df: pd.DataFrame = pd.read_html(str(table))[0]
        df.index = index
        df.to_json(config.JSON_NAME, force_ascii=False)

        # Excel作成
        print("        : Excelデータへ変換中")
        df = pd.read_json(config.JSON_NAME)
        df.to_excel(config.EXCEL_NAME, encoding=config.EXCEL_ENCODING)

        # ログアウト
        url = f'https://rokboard.com/logout'
        print('        : ROKボードからログアウト中')
        res = session.get(url)
        res.raise_for_status()
        print(f'        : ログアウト：{res.status_code}')

        # Discordに投稿
        print(f'        : Discordへ投稿中')
        embed = discord.Embed(title='ROK-BOARD DATA', description='データの作成に成功しました。')
        await ctx.send(embeds=[embed], files=[discord.File(config.EXCEL_NAME)])
        print(f'result  : 正常終了')

    except Exception as e:
        embed = discord.Embed(title="Error", description=e.__repr__())
        embed.add_field(name="Detail", value=str(e))
        print('\n■ Error Message')
        print(e)
        await ctx.send(embeds=[embed])
    print('\n-------------------------------------------------')


@slash.slash(name="get_governor_data", guild_ids=guild_ids,
             description="Gets the governor-information for the specified ID.")
async def _get_governor_data(ctx: SlashContext, governor_id: int):
    # Log
    now = datetime.now()
    now = str(now)[0:19]
    print(f'command : {ctx.command} governor_id:{governor_id}')
    print(f'time    : {now}')
    print(f'user    : {ctx.author.name}')

    try:
        debug = False

        if debug:
            has_permission = False
            for role in ctx.author.roles:
                if role.id in permission_role:
                    has_permission = True

            if not has_permission:
                print('権限がありません。')
                embed = discord.Embed(title='Maintenance', description='This bot is currently under maintenance.')
                await ctx.send(embeds=[embed])
                return

        df = pd.read_json(config.JSON_NAME)

        # 総督の存在チェック
        if governor_id not in df.index:
            print(f'result  : There is no information for the specified governor.')
            embed = discord.Embed(title="Error", description="There is no information for the specified governor.")
            await ctx.send(embeds=[embed])
            return

        import math
        governor = df.loc[governor_id]

        # 総督情報を取得
        governor_id = governor.name
        governor_name = governor.Governor[0:math.floor(len(governor.Governor) / 2)]
        governor_power = governor["Pwr"]
        governor_point = governor["Points"]
        governor_t4_kill = governor["T4 Kills"]
        governor_t5_kill = governor["T5 Kills"]
        governor_death = governor["Death Troops"]

        # 各項目の順位を取得
        rank = df.rank(ascending=False).loc[governor_id]
        rank_power = int(rank["Pwr"])
        rank_point = int(rank["Points"])
        rank_t4_kill = int(rank["T4 Kills"])
        rank_t5_kill = int(rank["T5 Kills"])
        rank_death = int(rank["Death Troops"])

        # ランク判定
        power_rank = rok.judge_rank(governor_death)

        # Discordに投稿
        new = True
        if new:
            # 次ランクまでの数値を判定
            embed = discord.Embed(title=f'{governor_name}', description=f'ID:{governor_id}')
            embed.add_field(name="Points\t" + f'({rank_point})', value='{:,}'.format(governor_point))
            embed.add_field(name="Power\t" + f'({rank_power})', value='{:,}'.format(governor_power), inline=False)
            embed.add_field(name="Death Troops\t " + f'({rank_death})', value='{:,}'.format(governor_death))
            embed.add_field(name="T4 Kills\t " + f'({rank_t4_kill})', value='{:,}'.format(governor_t4_kill),
                            inline=False)
            embed.add_field(name="T5 Kills\t " + f'({rank_t5_kill})', value='{:,}'.format(governor_t5_kill))
            embed.add_field(name="Power Rank", value=None if power_rank is None else power_rank.name, inline=False)
            df_date: pd.DataFrame = pd.read_json(config.UPD_DATE)
            embed.set_footer(
                text=f'item\t(rank)   update date : {df_date.loc[0].upd_date}\n\n※ The power rank is judged by 100% quota.')
            await ctx.send(embeds=[embed])

        print(f'result  : 正常終了')

    except Exception as e:
        embed = discord.Embed(title="Error", description=e.__repr__())
        embed.add_field(name="Detail", value=str(e))
        print('\n■ Error Message')
        print(traceback.format_exc())
        await ctx.send(embeds=[embed])
    print('\n-------------------------------------------------')


@bot.event
async def on_ready():
    print(f'We have logged in as {bot.user}')
    print('\n-------------------------------------------------')


print("botを起動します。")
bot.run(config.BOT_TOKEN)
